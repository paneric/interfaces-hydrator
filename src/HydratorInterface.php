<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Hydrator;

use Paneric\Interfaces\Converter\ConverterInterface;

interface HydratorInterface extends ConverterInterface
{
    public function hydrate(array $attributes);
}
